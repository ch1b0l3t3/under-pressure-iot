/**
 * Author: Danilo Carneiro de Souza.
 * =====================================================================
 * 
 * Custom implementation of a.k.a. UART.
 * 
 * =====================================================================
 * If you want to compile and install this custom USART you only need to
 * compile this file to produce a new custom_uart.o object. Link your 
 * target application with the generated 'custom_uart.o' instead of 'uart.o'. 
 * =====================================================================
 * compile:
 * ====================
 * 
 * 		// Optimized, system clock 16MHz, chip model (atmega328), and compile/assembly source
 *  	avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o uart/custom_uart.o uart.c
 * 
 */

#include <avr/io.h> // avr-libc header file that includes the apropriate IO definitions for atmega328 mmcu compiler command-line option.
#include <stdio.h>

#define F_CPU 16000000UL // AVR system clock rate of 16MHz (unsigned long format)
#define BAUD 9600 // Serial port baud rate

#include <util/setbaud.h> // avr-libc helper macros for baud rate calculations

void uart_init(void) {
	
	/*
	 * UBRR register is used to set UART speed.
	 * UBRRH_VALUE contains the upper byte (UBRRH) of the calculated prescaler value (for the UBRR register).
	 * UBRRL_VALUE contains the lower byte (UBRRL) of the calculated prescaler value (for the UBRR register).
	 */
    UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	
	/*
	 * Used to determine if UART has to be configured to run in double speed mode with given baud rate.
	 * Use macro USE_2X that set U2X contains the value 1 if the desired baud rate tolerance could only be achieved by
	 * setting the U2X bit in the UART configuration, otherwise contains 0.
	 * 
	 * Set U2X bit in the UART configuration (bit 1 of UCSRA status data register)
	 */
	#if USE_2X
	UCSR0A |= (1 << U2X0);
	#else
	UCSR0A &= ~(1 << U2X0);
	#endif
	
    UCSR0C |= (1<<UCSZ00) | (1<<UCSZ01); /* Set to 8-bit data */
    UCSR0B |= (1<<RXEN0) | (1<<TXEN0); /* Enable RX and TX */
    
}

void uart_putchar(char c, FILE *stream) {
	
    if (c == '\n') {
		uart_putchar('\r', stream);
	}
	
	/* 
	 * Wait until the port is ready to be written to.
	 * AVR Macro: loop_until_bit_is_set(UCSR0A, UDRE0);
	 */
	while((UCSR0A & (1 << UDRE0)) == 0 ){}
	UDR0 = c;
	
}

char uart_getchar(FILE *stream) {
	
	/* 
	 * Wait until the port is ready to be readed.
	 * AVR Macro: loop_until_bit_is_set(UCSR0A, RXC0);
	 */
	while((UCSR0A & (1 << RXC0)) == 0 ){}
	return UDR0;
	
}
