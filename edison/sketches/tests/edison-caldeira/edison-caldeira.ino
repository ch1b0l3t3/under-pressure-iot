/***********************************************************************
 * FILENAME : edison-caldeira.ino
 *
 * DESCRIPTION :
 *       Main file for AWS and physical components loop
 *       This program only works when called from LINUX BASH in a folder that has the "certs" (certificates) folder
 *
 * PUBLIC FUNCTIONS :
 *       void setup()
 *       void loop()
 *       int  main()
 * 
 *
 */

#ifndef __GLOBAL_DATA__
#define __GLOBAL_DATA__
#include "globalData.h"
#endif

/* Initiates each module
 * auto called once from the real main, generated on compilation
 */
void setup() {
    shouldContinue = true;
    fail = 0;
    
    //inits struct values for each module
    initPhysical();
    
    // inits configuration
    loopDelay = 1000;
    loopCount = 0;
    
    //shadow communication
    shadowConfig();
    shadowConnect();
    
}

/* Calls updates from each module in a collect, process and execute fashion
 * auto called repeatedly from the real main, generated on compilation
 */
bool myLoop() {
    
    //read from sensors
    sensorPhysicalUpdate();

    // communicate with cloud
	shadowUpdate();
    
    //apply decisions to actuators
    actuatorPhysicalUpdate();
    
    delay(loopDelay);
    loopCount++;

    //return shouldContinue;
  return true;
}


int main()
{
  char * filler[1];
  filler[0] = "";
  
  init(0,filler );
  setup();
  while(myLoop());
  IOT_ERROR("Too many fails, exiting...");
  return 0;
}



