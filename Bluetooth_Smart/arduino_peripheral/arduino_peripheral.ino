/*
 * Arduino Periferal: Temperature sensor and BLE communication with KEYES HM-10 BLE module.
 * 
 * Pins:
 *  BLE VCC to Arduino 3.3V out
 *  BLE GND to GND
 *  Arduino 8 (SS RX) - BLE TX
 *  Arduino 7 (SS TX) - BLE RX
 *  Arduino 13        - BLE STATE
 *  Arduino 4         - BLE BRK
 *  Arduino 12        - Temperature Sendor Signal
 *  Temperature Sensor VCC to Arduino 5V out
 *  Temperature Sensor GND to Arduino GND
 */

#include <SoftwareSerial.h>
#include <DHT.h>
#include <avr/wdt.h>

/**
 * Define sleep modes
 */
#define IDLE_MODE 0
#define POWER_DOWN_MODE 1
#define POWER_SAVE_MODE 2
#define STANDBY_MODE 3
#define EXT_STANDBY_MODE 4

#define UART_RX_PIN 7
#define UART_TX_PIN 8
#define DHTPIN 12
#define DHTTYPE DHT11

/**
 * Set up serial interface for BLE
 */
SoftwareSerial bleSerial(UART_TX_PIN, UART_RX_PIN); // RX, TX

/**
 * Set up temperature sensor and variables
 */
DHT dht(DHTPIN, DHTTYPE);

const int knockSensor = A2; // the piezo is connected to analog pin 0
int sensorReading = 0;      // variable to store the value read from the sensor pin

float valueH;
float valueT;

/*
 * Sleep mode inline call
 */
void sleep_cpu() {
  asm ( "sleep" );
}

/*
 * Set up SMCR register (Sleep Mode Control Register)
 * char* SMCR = (char*) 0x053;
 */
void set_sleep_mode(int mode) {
  
  switch (mode) {
    case IDLE_MODE:
      SMCR |= 0b00000001;
      break;
    case POWER_DOWN_MODE:
      SMCR |= 0b00000101;
      break;
    case POWER_SAVE_MODE:
      SMCR |= 0b00000111;
      break;
    case STANDBY_MODE:
      SMCR |= 0b00001101;
      break;
    case EXT_STANDBY_MODE:
      SMCR |= 0b00001111;
      break;
  }
  
}

/**
 * Set up watchdog timer
 */
void watchdogSetup() {
  
  cli();  // disable all interrupts
  wdt_reset(); // reset the WDT timer

  /* == Set up watchdog ================= */
  MCUSR &= ~(1 << WDRF); // reset status flag
  WDTCSR |= (1 << WDCE) | (1 << WDE); // enable configuration changes (thru four clock cycles)
  WDTCSR = (1 << WDP3) | (1 << WDP0);
  WDTCSR |= (1 << WDIE);  // enable WDT 
  
  sei(); // enable interrupts
  
}

/**
 * Watchdog timer interrupt
 */
ISR(WDT_vect) {

  Serial.print(sensorReading);     // print value
  Serial.println(" m/s");    // print value
  
  Serial.print(valueT);     // print value
  Serial.println(" *C");    // print value
    
  bleSerial.print(sensorReading);  // update BLE Characteristic
    
}

void setup() {

  pinMode(13, INPUT);           // BLE STATE pin
  pinMode(4, OUTPUT);           // BLE BRK pin

  Serial.begin(9600);           // set the baud rate to 9600 - Serial console

  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(" ");
  
  bleSerial.begin(9600);         // set the baud rate to 9600 - BLE device

  Serial.println("* Serial communication started at 9600 *");

  bleSerial.print("AT+NAMEBLE-UP-IOT");

  digitalWrite(4, HIGH); // set up BRK signal to avoid connections
  delay(1000);
  
  watchdogSetup();

}

void loop() {

  int i = 0;
  for(i = 0; i < 100; i++) {
    valueH = dht.readHumidity();
    valueT = dht.readTemperature();
    sensorReading = analogRead(knockSensor); // use a inline resistor to reduce scale
    delay(10);
  }
  
  set_sleep_mode(1); // Enable sleep mode - Power-Down mode
  sleep_cpu(); // Enter in sleep mode

  delay(1000);
  
}
