/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * @file shadow_sample.c
 * @brief A simple connected window example demonstrating the use of Thing Shadow
 */



/*!
 * The goal of this sample application is to demonstrate the capabilities of shadow.
 * This device(say Connected Window) will open the window of a room based on temperature
 * It can report to the Shadow the following parameters:
 *  1. temperature of the room (double)
 *  2. status of the window (open or close)
 * It can act on commands from the cloud. In this case it will open or close the window based on the json object "windowOpen" data[open/close]
 *
 * The two variables from a device's perspective are double temperature and bool windowOpen
 * The device needs to act on only on windowOpen variable, so we will create a primitiveJson_t object with callback
 The Json Document in the cloud will be
 {
 "reported": {
 "temperature": 0,
 "windowOpen": false
 },
 "desired": {
 "windowOpen": false
 }
 }
 */

#ifndef __GLOBAL_DATA__
#define __GLOBAL_DATA__
#include "globalData.h"
#endif


void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                const char *pReceivedJsonDocument, void *pContextData) {
  IOT_UNUSED(pThingName);
  IOT_UNUSED(action);
  IOT_UNUSED(pReceivedJsonDocument);
  IOT_UNUSED(pContextData);

  if(SHADOW_ACK_TIMEOUT == status) {
    IOT_INFO("Update Timeout--");
  } else if(SHADOW_ACK_REJECTED == status) {
    IOT_INFO("Update RejectedXX");
  } else if(SHADOW_ACK_ACCEPTED == status) {
    IOT_INFO("Update Accepted !!");
  }
}

void general_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  IOT_UNUSED(pJsonString);
  IOT_UNUSED(JsonStringDataLen);

  if(pContext != NULL) {

  }

}

void target_temp_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
  IOT_UNUSED(pJsonString);
  IOT_UNUSED(JsonStringDataLen);

  if(pContext != NULL) {
  }
}
    
void shadowConfig() 
{
  sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

  waterTemp.cb = general_Callback;
  waterTemp.pData = &caldronSystem.waterTemp;
  waterTemp.pKey = "waterTemp";
  waterTemp.type = SHADOW_JSON_FLOAT;

  tempLowerLimit.cb = general_Callback;
  tempLowerLimit.pData = &caldronSystem.tempLowerLimit;
  tempLowerLimit.pKey = "tempLowerLimit";
  tempLowerLimit.type = SHADOW_JSON_FLOAT;
  
  tempUpperLimit.cb = general_Callback;
  tempUpperLimit.pKey = "tempUpperLimit";
  tempUpperLimit.pData = &caldronSystem.tempUpperLimit;
  tempUpperLimit.type = SHADOW_JSON_FLOAT;

  pipe1ShouldBeOpen.cb = general_Callback;
  pipe1ShouldBeOpen.pKey = "pipe1ShouldBeOpen";
  pipe1ShouldBeOpen.pData = &distributionSystem.pipe1ShouldBeOpen;
  pipe1ShouldBeOpen.type = SHADOW_JSON_BOOL;

  pipe2ShouldBeOpen.cb = general_Callback;
  pipe2ShouldBeOpen.pKey = "pipe2ShouldBeOpen";
  pipe2ShouldBeOpen.pData = &distributionSystem.pipe2ShouldBeOpen;
  pipe2ShouldBeOpen.type = SHADOW_JSON_BOOL;

  pipe3ShouldBeOpen.cb = general_Callback;
  pipe3ShouldBeOpen.pKey = "pipe3ShouldBeOpen";
  pipe3ShouldBeOpen.pData = &distributionSystem.pipe3ShouldBeOpen;
  pipe3ShouldBeOpen.type = SHADOW_JSON_BOOL;

  IOT_INFO("\nAWS IoT SDK Version %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

  //parseInputArgsForConnectParams(argc, argv);

  getcwd(CurrentWD, sizeof(CurrentWD));
  snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
  snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
  snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

  IOT_INFO("rootCA %s", rootCA);
  IOT_INFO("clientCRT %s", clientCRT);
  IOT_INFO("clientKey %s", clientKey);

  // initialize the mqtt client

  sp = ShadowInitParametersDefault;
  sp.pHost = AWS_IOT_MQTT_HOST;
  sp.port = AWS_IOT_MQTT_PORT;
  sp.pClientCRT = clientCRT;
  sp.pClientKey = clientKey;
  sp.pRootCA = rootCA;
  sp.enableAutoReconnect = false;
  sp.disconnectHandler = NULL;
    
}


void shadowConnect()
{
  IOT_INFO("Shadow Init");
  rc = aws_iot_shadow_init(&mqttClient, &sp);
  if(SUCCESS != rc) {
  IOT_ERROR("aws_iot_shadow_init exit with: %d", rc);
    IOT_ERROR("Shadow Connection Error");
    return;
  }

  scp = ShadowConnectParametersDefault;
  scp.pMyThingName = AWS_IOT_MY_THING_NAME;
  scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
  scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);

  IOT_INFO("Shadow Connect");
  rc = aws_iot_shadow_connect(&mqttClient, &scp);
  if(SUCCESS != rc) {
    IOT_ERROR("Shadow Connection Error");
    return;
  }

  /*
   * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
   *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
   *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
   */
  rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
  if(SUCCESS != rc) {
    IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
    return;
  }

  rc = aws_iot_shadow_register_delta(&mqttClient, &waterTemp);
  rc = aws_iot_shadow_register_delta(&mqttClient, &tempLowerLimit);
  rc = aws_iot_shadow_register_delta(&mqttClient, &tempUpperLimit);

  rc = aws_iot_shadow_register_delta(&mqttClient, &pipe1ShouldBeOpen);
  rc = aws_iot_shadow_register_delta(&mqttClient, &pipe2ShouldBeOpen);
  rc = aws_iot_shadow_register_delta(&mqttClient, &pipe3ShouldBeOpen);


  if(SUCCESS != rc) {
    IOT_ERROR("Shadow Register Delta Error");
  }
  //temperature = STARTING_ROOMTEMPERATURE;
}

// after some failed attempts, returns false to kill itself
bool shadowFailed()
{
  if(SUCCESS != rc) {
    IOT_ERROR("An error occurred in the loop %d", rc);
  }

  IOT_INFO("Disconnecting");
  rc = aws_iot_shadow_disconnect(&mqttClient);

  if(SUCCESS != rc) {
    IOT_ERROR("Disconnect error %d", rc);
  }

  shadowConnect();

  fail++;

  if (fail > 3)
  {
    return false;
  }
  else
  {
    return true;
  }
}

// after some failed attempts, returns false to kill itself
bool shadowUpdate()
{
	static int alternateData = 0;
  // loop and publish a change in temperature
  if(NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc) {
    rc = aws_iot_shadow_yield(&mqttClient, 200);
    if(NETWORK_ATTEMPTING_RECONNECT == rc) {
      sleep(1);
      // If the client is attempting to reconnect we will skip the rest of the loop.
      return true;
    }
    IOT_INFO("\n=======================================================================================\n");
    //IOT_INFO("On Device: window state %s", windowOpen ? "true" : "false");


    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
    if(SUCCESS == rc) {

		if (alternateData % 2 == 1)
		{
			rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 3, &waterTemp, &tempLowerLimit, &tempUpperLimit);
		}
		else
		{
			rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 3, &pipe1ShouldBeOpen, &pipe2ShouldBeOpen, &pipe3ShouldBeOpen);
		}

      if(SUCCESS == rc) {
        rc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
        if(SUCCESS == rc) {
          IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
		  alternateData++;
          rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                         ShadowUpdateStatusCallback, NULL, 4, true);
        }
      }
    }
    IOT_INFO("*****************************************************************************************\n");
    fail = 0;
    return true;
  }
    else
    {
        if (shadowFailed() == false)
        {
          return false;
        }
        else
        {
          delay(2000);
          shadowConnect();
          return true;
        }
    }
}



