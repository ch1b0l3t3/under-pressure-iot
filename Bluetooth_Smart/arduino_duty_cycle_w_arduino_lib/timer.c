/**
 * Author: Danilo Carneiro de Souza.
 * =====================================================================
 * 
 * TIMER (Arduino Uno - TIMER0)
 * 
 * =====================================================================
 * compile:
 * ====================
 * 
 * 		// Optimized, system clock 16MHz, chip model (atmega328), and compile/assembly source
 *  	avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o timer/timer.o timer.c
 * 
 */

#include <stdio.h>
#include <avr/interrupt.h>  // avr-libc interrupt library

volatile unsigned long act_time_ms = 0; // Active time

void setup_timer(void) {
	
	/*
	 * Set counter to verify match to 1 sec equivalent pulses.
	 * Each interrupt represents 1 millisecond elipsed.
	 */
	TCNT0 = 0x00; // Set timer0 (global timer) counter initial value to 0
	TCCR0B = (1 << CS01) | (1 << CS00); // Set timer0 with 64 factor prescaler
	OCR0A = 0xFA; // Set to 250 (measure 250 pulses/milliseconds)
	TIMSK0 |= (1 << 1); // Enable the interrupt on compare match OCR0A for Timer0 (global timer)
	
}

unsigned long get_active_time_ms(void) {
	
	return act_time_ms;
	
}

/*
 * =============================
 * ISR routines implementations.
 * =============================
 */

/*
 * Interrupt-based global timer ISR-TIMER0_COMPA
 */
ISR(TIMER0_COMPA_vect){
	
	act_time_ms++;
	TCNT0 = 0x00; // Set timer0 counter value to 0
	
}
