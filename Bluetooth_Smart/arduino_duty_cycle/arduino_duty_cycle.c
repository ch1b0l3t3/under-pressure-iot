/**
 * Author: Danilo Carneiro de Souza.
 * 
 * =====================================================================
 * Duty Cycle for Arduino
 * ======================
 * =====================================================================
 * Notes:
 * ======
 * 
 * project structure:
 * 
 * arduino_duty_cycle/
 * 		arduino_duty_cycle.c
 *  	uart.c
 *  	led.c
 *  	timer.c
 *  	watchdog.c
 * 		README
 * 		Makefile
 * 		uart/
 * 			uart.h
 * 		led/
 * 			led.h
 * 		timer/
 * 			timer.h
 * 		watchdog/
 * 			watchdog.h
 * 
 * =====================================================================
 * Compile and Install:
 * ====================
 * 
 * 	Please, find enclosed to the project folder the Makefile prepared
 * 	to compile and link this project.
 * 
 * 	Use:	make			// compile/link
 * 			make clean		// remove all generated files
 * 			make clean-obj	// remove only the generated object files
 * 			make install	// install application on Arduino (attempt to use the correct serial port - default /dev/ttyACM0)
 * 
 * =====================================================================
 * To use a custom USART:
 * ======================
 * 
 * You're already using a custom implementation of a.k.a. UART.
 * 
 * To use the 'simple_uart.o' available from course module 741 (module-01) use the
 * simple_uart.o instead from 'uart' folder.
 * 
 * =====================================================================
 * Command line instructions:
 * ==========================
 * 
 * avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o uart/custom_uart.o uart.c
 * avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o led/led.o led.c
 * avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o timer/timer.o timer.c
 * avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o arduino_duty_cycle.o arduino_duty_cycle.c
 * avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o watchdog/watchdog.o watchdog.c
 * avr-gcc -mmcu=atmega328 uart/uart.o led/led.o switcher/switcher.o timer/timer.o watchdog/watchdog.o arduino_duty_cycle.o -o arduino_duty_cycle
 * 
 * avr-objcopy -O ihex -R .eeprom arduino_duty_cycle arduino_duty_cycle.hex
 * avrdude -F -V -C ${ARDUINO_PATH}/hardware/tools/avr/etc/avrdude.conf -c arduino -p ATMEGA328 -P /dev/ttyACM0 -b 115200 -U flash:w:arduino_duty_cycle.hex
 * 
 * =====================================================================
 * Compile and Install:
 * ====================
 * 
 * 	Please, find enclosed to the project folder the Makefile prepared
 * 	to compile and link this project.
 * 
 * 	Use:	make			// compile/link
 * 			make clean		// remove all generated files
 * 			make clean-obj	// remove only the generated object files
 * 			make install	// install application on Arduino (attempt to use the correct serial port - default /dev/ttyACM0)
 * 
 * =====================================================================
 * 
 */


#define IDLE_MODE 0
#define POWER_DOWN_MODE 1
#define POWER_SAVE_MODE 2
#define STANDBY_MODE 3
#define EXT_STANDBY_MODE 4
#define __DELAY_BACKWARD_COMPATIBLE__

#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>  // avr-libc interrupt library
#include "uart/uart.h" // USART tty serial port access support
#include "led/led.h"  // custom LED library (pin 13)
#include "timer/timer.h"  // custom millis counter TIMER0 library
#include "watchdog/watchdog.h"  // custom millis counter TIMER0 library

char* led; // GPIO 8-bit I/O port mapped to pin 13 (microcontroller PB5 port - attached to led) on Arduino. 
char* sreg = (char*) 0x05F; // SREG register (for enabling interruptions instead of use sei() function)

/*
 * Sleep mode inline call
 */
void sleep_cpu() {
	asm ( "sleep" );
}

/*
 * Set up SMCR register (Sleep Mode Control Register)
 * 
 * char* SMCR = (char*) 0x053;
 */
void set_sleep_mode(int mode) {
	
	switch (mode) {
		case IDLE_MODE:
			SMCR |= 0b00000001;
			break;
		case POWER_DOWN_MODE:
			SMCR |= 0b00000101;
			break;
		case POWER_SAVE_MODE:
			SMCR |= 0b00000111;
			break;
		case STANDBY_MODE:
			SMCR |= 0b00001101;
			break;
		case EXT_STANDBY_MODE:
			SMCR |= 0b00001111;
			break;
	}
	
}

/*
 * =============================
 * Support variables.
 * =============================
 */

double delayTime = 10.0f;
/* Define interval - default: 40ms (only to produce a bigger active time) - APPLY '0.0' TO SUPPRESS IT - */
unsigned long working_time = 1000.0;

/*
 * =============================
 * ISR routines implementations.
 * =============================
 */

/*
 * Switcher ISR-INT0
 */
ISR (INT0_vect){
	
	/*
	 * Print computed information.
	 */
	printf("\n** Active time: %lu ms ** \n", get_active_time_ms());
	printf("** Time since boot: %lu ms ** \n", get_total_time_ms());
	
	_delay_ms(working_time);
	
}

/*
 * ====================
 * PROGRAM entry point.
 * ====================
 */

int main(void) {
	
	/*
	 * Set up LED/timer libraries
	 */
	led = setup_led();
	setup_timer();
	
	/*
	 * Initialize UART
	 */
	uart_init();
	stdout = &uart_output;
	stdin  = &uart_input;
	
	/* Only to signal reset events */
	puts("System up \n");
	_delay_ms(delayTime);
	
	*sreg |= (1 << 7); // Enable global interrupts pin
	
	while(1) {
		
		*led &= ~(1 << 5); // Set pin 13 to initial LOW state (turn off LED)
		
		set_sleep_mode(1); // Enable sleep mode - default: Power-Down mode
		
		setup_watchdog(led); // Set up WDT register - default: 16 (125ms)
		sleep_cpu(); // Enter in sleep mode
		
	}
	
	return 0;
	
}
