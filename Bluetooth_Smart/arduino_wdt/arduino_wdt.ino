#include <avr/wdt.h>

void(* resetFunc) (void) = 0; //declare reset function @ address 0

void watchdogSetup() {
  
  cli();  // disable all interrupts
  wdt_reset(); // reset the WDT timer

  /* == Set up watchdog ================= */
  MCUSR &= ~(1 << WDRF); // reset status flag
  WDTCSR |= (1 << WDCE) | (1 << WDE); // enable configuration changes (thru four clock cycles)
  WDTCSR = (1 << WDP3) | (1 << WDP0);
  WDTCSR |= (1 << WDIE);  // enable WDT 
  
  sei();
  
  Serial.println("finished watchdog setup");  // just here for testing
  
}


ISR(WDT_vect) // Watchdog timer interrupt.
{
    delay(5);
    Serial.println("Howdy");
}


void setup(){
  watchdogSetup();
  Serial.begin(9600);
  delay(2);
}

int firstTime = true;

void loop() {
  
  if (firstTime){
    firstTime = false;
    Serial.println("In loop waiting for Watchdog");
  }
  
}
