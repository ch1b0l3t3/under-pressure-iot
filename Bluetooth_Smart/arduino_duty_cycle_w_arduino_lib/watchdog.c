/**
 * Author: Danilo Carneiro de Souza.
 * =====================================================================
 * 
 * WATCHDOG (Arduino Uno - WDT)
 * 
 * =====================================================================
 * compile:
 * ====================
 * 
 * 		// Optimized, system clock 16MHz, chip model (atmega328), and compile/assembly source
 *  	avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o timer/timer.o timer.c
 * 
 */

#define FACTOR 16 // prescaler factor

#include <Arduino.h>
#include <stdio.h>
#include <avr/interrupt.h>  // avr-libc interrupt library
#include <avr/wdt.h>  // avr-libc watchdog timer library

volatile unsigned long milliseconds = 0; // Global total time (ms)
volatile unsigned long millisecs = 0; // Local total time (ms)
volatile unsigned long seconds = 0; // Global total time (s)
volatile unsigned long ms_factor; // WDT time step (related to the selected prescaler)

char* led; // ref to.: GPIO 8-bit I/O port mapped to pin 13 (microcontroller PB5 port - attached to led) on Arduino. 

/*
 * GPIO 8-bit I/O port mapped to pin 2 (microcontroller PD2 port INT0 for external interrupt) on Arduino. 
 */
char* pinD = (char*) 0x029;
char* ddrD = (char*) 0x02A;
char* portD = (char*) 0x02B;

char* eicra = (char*) 0x069; // EICRA register (for configure interrupt on INT0 pin - pin 2)
char* eimsk = (char*) 0x03D; // EIMSK register (for enabling interrupts on INT0 pin - pin 2)

/*
 * Set up SMCR register (Sleep Mode Control Register)
 * 
 * char* MCUSR = (char*) 0x054;
 * char* WDTCSR = (char*) 0x060;
 * WDRF =	MCUSR |= (1 << 3);
 * WDE  =	WDTCSR |= (1 << 3);
 * WDCE =  	WDTCSR |= (1 << 4);
 * WDIE =  	WDTCSR |= (1 << 6);
 * WDP0 =  	WDTCSR |= (1 << 0);
 * WDP1 =  	WDTCSR |= (1 << 1);
 * WDP2 =  	WDTCSR |= (1 << 2);
 * WDP3 =  	WDTCSR |= (1 << 5);
 */
void setup_watchdog(char* p) {
	
	*led = (char*) *p;
	
	/* == Set up interrupt ================ */
	/*
	 * PD2 (external interrupt on pin 2) configuration
	 */
	*ddrD &= ~(1 << 2); // Makes pin 2 (PD2) of port D as input
	
	/*
	 * Button interrupt configuration
	 */
	*portD |= (1 << 2); // PD2 is now with pull-up enabled
	*eicra |= (1 << 1) | (1 << 0); // Set to generates an interrupt request on rising edge of INT0
	*eimsk |= (1 << 0); // Enable INT0 interrupt
	
	/* == Set up watchdog ================= */
	MCUSR &= ~(1 << WDRF); // reset status flag
	WDTCSR |= (1 << WDCE) | (1 << WDE); // enable configuration changes (thru four clock cycles)
	
	switch (FACTOR) {
		case 4:
			WDTCSR = (1 << WDP0);
			ms_factor = 32;
			//32ms
			break;
		case 8:
			WDTCSR = (1 << WDP1);
			ms_factor = 64;
			//64ms
			break;
		case 16:
			WDTCSR = (1 << WDP1) | (1 << WDP0);
			ms_factor = 125;
			//0.125s
			break;
		case 32:
			WDTCSR = (1 << WDP2);
			ms_factor = 250;
			//0.25s
			break;
		case 64:
			WDTCSR = (1 << WDP2) | (1 << WDP0);
			ms_factor = 500;
			//0.5s
			break;
		case 128:
			WDTCSR = (1 << WDP2) | (1 << WDP1);
			ms_factor = 1000;
			//1.0s
			break;
		case 256:
			WDTCSR = (1 << WDP2) | (1 << WDP1) | (1 << WDP0);
			ms_factor = 2000;
			//2.0s
			break;
		case 512:
			WDTCSR = (1 << WDP3);
			ms_factor = 4000;
			//4.0s
			break;
		case 1024:
			WDTCSR = (1 << WDP3) | (1 << WDP0);
			ms_factor = 8000;
			//8.0s
			break;
		default:
			WDTCSR = 0b00000000;
			ms_factor = 16;
			//16ms
			break;
	}
	
	WDTCSR |= (1 << WDIE);  // enable WDT 
	
}

unsigned long get_total_time_ms(void) {
	
	return milliseconds;
	
}

unsigned long get_ms_factor(void) {
	
	return ms_factor;
	
}

/*
 * =============================
 * ISR routines implementations.
 * =============================
 */

/*
 * Watchdog interrut mode ISR-WDT
 */
ISR (WDT_vect){
	
	milliseconds += ms_factor;
	millisecs += ms_factor;
	if(millisecs >= 1000) {
		seconds++;
		millisecs = 0;
		if(seconds >= 8) {
			seconds = 0;
			//*led |= (1 << 5); // Set pin 13 ref to HIGH state (turn on LED - means system is up)
			digitalWrite(13, HIGH);
		}
	}
	
}
