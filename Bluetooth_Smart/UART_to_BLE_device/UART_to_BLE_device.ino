/*
 * Arduino to BLE communication using Serial Commands
 * 
 * Use through UART:
 *  "AT" to get BLE "ATOK" if all works fine.
 *  "AT+NAME?" to get BLE name as response.
 * 
 * Pins:
 *  BLE VCC to Arduino 3.3V out. 
 *  BLE GND to GND
 *  Arduino D8 (SS RX) - BLE TX
 *  Arduino D7 (SS TX) - BLE RX
 *  Arduino D13        - BLE STATE
 *  Arduino D4         - BLE BRK
 */

#include <SoftwareSerial.h>

SoftwareSerial bleSerial(8, 7); // RX, TX

char junk = ' ';
boolean newLine = true;

void setup() {
  
  pinMode(13, INPUT);           // BLE STATE pin
  pinMode(4, OUTPUT);           // BLE BRK pin

  digitalWrite(4, LOW);         // prevent connections

  Serial.begin(9600);           // set the baud rate to 9600

  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  Serial.println(" ");
  
  bleSerial.begin(9600);         // set the baud rate to 9600

  Serial.println("* Serial communication started at 9600 *");

}

void loop() {

  // Read from the Bluetooth module and send to the Arduino Serial Monitor
  if(bleSerial.available()) {

    while(bleSerial.available()) {
      
      junk = bleSerial.read();
      Serial.write(junk);
      
    }
    
  }
  
  // Read from the Serial Monitor and send to the Bluetooth module
  while(Serial.available()) {

    // Echo the user input to the main window. 
    // If there is a new line print the ">" character.
    if(newLine) {
      Serial.print("\r\n>");
      newLine = false;
    }

    junk = Serial.read();

    // do not send line end characters to the HM-10
    if (junk!=10 & junk!=13 ) {  
      bleSerial.write(junk);
    }

    Serial.write(junk);

    if(junk == 10) {
      newLine = true;
    }
    
  }
  
}
