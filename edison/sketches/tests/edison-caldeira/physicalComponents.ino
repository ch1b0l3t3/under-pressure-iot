/***********************************************************************
 * FILENAME : physicalComponents.ino
 *
 * DESCRIPTION :
 *       Collects data from physical sensors and executes logic
 *
 * PUBLIC FUNCTIONS :
 *       void initPhysical()
 *       void sensorPhysicalUpdate()
 *       void actuatorPhysicalUpdate()
 * 
 *
 */
#include <TH02_dev.h>
#include <Wire.h>
#include "rgb_lcd.h"

#ifndef __GLOBAL_DATA__
#define __GLOBAL_DATA__
#include "globalData.h"
#endif


#define PIN_SERVO 6

#define PIN_RELAY_1 3
#define PIN_RELAY_2 4
#define PIN_RELAY_3 8

rgb_lcd lcd;
Servo servo;


/**********************************
 * ACTUATORS LOGIC
 **********************************/

/* Updates LCD with system state
 */
void lcdPrintSysState()
{
  char strTopLine[17];
  char strBottomLine[17];
    
    snprintf(strTopLine, 17, "temp: %2.2f", caldronSystem.waterTemp);
    snprintf(strBottomLine, 17, "");
    
    lcd.clear();
    
    lcd.setCursor(0, 0);
    lcd.print(strTopLine);
    
    lcd.setCursor(0, 1);
    lcd.print(strBottomLine);
}

/* Rotate servo accordingly to AWS configuration
 *
 */
void servoUpdate()
{
    if (caldronSystem.waterTemp < caldronSystem.tempLowerLimit)
    {
        servo.write(0);
    }
    else if (caldronSystem.waterTemp > caldronSystem.tempUpperLimit)
    {
        servo.write(90);
    }
    else
    {
      float angle =  (
          (caldronSystem.waterTemp - caldronSystem.tempLowerLimit) /
          (caldronSystem.tempUpperLimit - caldronSystem.tempLowerLimit)
        ) * 90.0;
      servo.write( (int)angle );
    }

  //servo.write(physicalComponents);
}


/**********************************
 * EXTERNALLY CALLABLE FUNCTIONS
 **********************************/

/* Initiates struct and prints info about this module
 * Should be called once at setup
 */
void initPhysical()
{

    //memset(&physicalComponents, 0, sizeof(physicalComponents) );

    memset(&caldronSystem, 0, sizeof(caldronSystem) );

    servo.attach(PIN_SERVO);

    lcd.begin(16, 2);
    lcd.print("LCD -- OK");

    caldronSystem.tempUpperLimit = 30.0;
    caldronSystem.tempLowerLimit = 28.0;

    delay(1000);

    memset(&distributionSystem, 0, sizeof(distributionSystem) );

    pinMode(PIN_RELAY_1,OUTPUT);
    digitalWrite(PIN_RELAY_1, HIGH);

    pinMode(PIN_RELAY_2,OUTPUT);
    digitalWrite(PIN_RELAY_2, HIGH);

    pinMode(PIN_RELAY_3,OUTPUT);
    digitalWrite(PIN_RELAY_3, LOW);

}

/* Reads and stores data from sensors
 * Should be called on every loop before updating system status
 */
void sensorPhysicalUpdate()
{
      // temperature sensor
      caldronSystem.waterTemp = TH02.ReadTemperature();

}

/* Activate actuators accordingly to system state
 * Should be called on every loop after updating system status
 */
void actuatorPhysicalUpdate()
{

    // print to lcd
    lcdPrintSysState();

    // rotate servo
    servoUpdate();

     digitalWrite(PIN_RELAY_1, distributionSystem.pipe1ShouldBeOpen ? LOW : HIGH);
     digitalWrite(PIN_RELAY_2, distributionSystem.pipe2ShouldBeOpen ? LOW : HIGH);
     digitalWrite(PIN_RELAY_3, distributionSystem.pipe3ShouldBeOpen ? HIGH : LOW);

}



