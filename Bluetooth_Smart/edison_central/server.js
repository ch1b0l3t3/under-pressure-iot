/**
 * GATT client
 * search for a specific service and characteristic
 */

var noble = require('noble');				// Load GATT client module
var request = require('request');		// Load http request module

// unable duplicated advertising - default false
var allowDuplicates = false;

/**
 * Desired services
 */
var serviceUuids = ['ffe0', 'ffe1'];

/**
 * Start and stop scan functions
 */
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning(serviceUuids, allowDuplicates);
  } else {
    noble.stopScanning();
  }
});

/**
 * Set discover behavior
 */
noble.on('discover', function(peripheral) {
  
  console.log();
  console.log('name: ' + peripheral.advertisement.localName + ' - address ' + peripheral.address + ' - RSSI ' + peripheral.rssi);
  console.log('Advertised Services: ' + JSON.stringify(peripheral.advertisement.serviceUuids));
  
	/**
	 * Connect to discovered device (scan specific service)
	 */
	peripheral.connect(function(error) {
		if(error) console.log(error);
		
		console.log('connected to peripheral: ' + peripheral.uuid);
		
		/**
		 * Discover service details
		 */
		peripheral.discoverServices(serviceUuids[0], function(error, services) {
			if(error) console.log(error);
			
			var deviceInformationService = services[0];
			
			/**
			 * Search for specific characteristic
			 */
			deviceInformationService.discoverCharacteristics(serviceUuids[1], function(error, characteristics) {
				if(error) console.log(error);
				
				var customCharacteristic;
				
				for (var i in characteristics) {
					if(error) console.log(error);
					
					customCharacteristic = characteristics[i];
					
					/**
					 * Handle data reading
					 */
					customCharacteristic.on('data', function(data, isNotification) {
					
						console.log('*******************************');
						console.log('characteristic value: ', data.toString('utf8') + ' *C');
						console.log('******************************');
						
						const WRITE_API_KEY = 'VWHU19CBO0PLWB8G';
						const READ_API_KEY = 'AN2C9TZ5V5KP7Y64';

						// Post to ThinkSpeak
						const options = {
							url: 'https://api.thingspeak.com/update?api_key=' + WRITE_API_KEY + '&field1=' + data.toString('utf8'),
							method: 'GET'
						};

						request(options, (error, response, body) => {
							if(error) console.log('error: ', error);
							if(response.statusCode != 200) console.log('error: ' + response.statusCode);
							
							/**
							 * Disconnect from device
							 */
							peripheral.disconnect(function(error) {
								
								if(error) console.log(error);
								console.log('disconnected from peripheral: ' + peripheral.uuid + ' ...bye!');
								
								setTimeout(function() {
									noble.startScanning(serviceUuids, allowDuplicates); // -- scan devices again --
								}, 1000);
								
							}); // -- disconnect --
							
						}); // -- ThingSpeak call --
					
					}); // -- on data --

					/**
					 * Subscribe to get notifications
					 */
					customCharacteristic.subscribe(function(error) {
						console.log('characteristics ' + characteristics[i].uuid);
						console.log('found :         ok');
						console.log('notification :  on');
					}); // -- subscribe --
					
				} // -- for --
				
			}); // -- discover charactiristics --

		}); // -- discover services --
		
	}); // -- connect --
	
}); // -- discover --
