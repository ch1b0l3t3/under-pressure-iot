/***********************************************************************
 * FILENAME : globalData.h
 *
 * DESCRIPTION :
 *       Centralizes data, types and configuration from modules
 * 
 *
 */

/**********************************
 * CONFIGURATION
 **********************************/

#define MIN_LOOP_DELAY 500
int loopDelay;

int loopCount;

bool shouldContinue;
int fail;

/**********************************
 * DATA STRUCTS
 **********************************/

// All data processed from modules inputs
// This is the core data struct, wich holds the system state

struct
{
  float waterTemp;
  float tempLowerLimit;
  float tempUpperLimit;
} caldronSystem;

struct
{
  bool pipe1ShouldBeOpen;
  bool pipe2ShouldBeOpen;
  bool pipe3ShouldBeOpen;
} distributionSystem;

/**********************************
 * PHYSICAL COMPONENTS
 **********************************/
 
#include<Servo.h>


/**********************************
 * AWS SHADOW 
 **********************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"
 
#define ROOMTEMPERATURE_UPPERLIMIT 32.0f
#define ROOMTEMPERATURE_LOWERLIMIT 25.0f
#define STARTING_ROOMTEMPERATURE ROOMTEMPERATURE_LOWERLIMIT

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 200

size_t sizeOfJsonDocumentBuffer;
static char certDirectory[] = "certs";
static char HostAddress[255] = AWS_IOT_MQTT_HOST;
static uint32_t port = AWS_IOT_MQTT_PORT;
static uint8_t numPubs = 5;
 
IoT_Error_t rc = FAILURE;
int32_t i = 0;
char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
char *pJsonStringToUpdate;
float temperature = 0.0;
bool windowOpen = false; 

jsonStruct_t waterTemp;  
jsonStruct_t tempLowerLimit;
jsonStruct_t tempUpperLimit;
jsonStruct_t pipe1ShouldBeOpen;
jsonStruct_t pipe2ShouldBeOpen;
jsonStruct_t pipe3ShouldBeOpen;

char rootCA[PATH_MAX + 1];
char clientCRT[PATH_MAX + 1];
char clientKey[PATH_MAX + 1];
char CurrentWD[PATH_MAX + 1];

AWS_IoT_Client mqttClient;   

ShadowInitParameters_t sp;
ShadowConnectParameters_t scp;


