/**
 * Author: Danilo Carneiro de Souza.
 * =====================================================================
 * 
 * LED (Arduino Uno - pin 13 PB5).
 * 
 * =====================================================================
 * compile:
 * ====================
 * 
 * 		// Optimized, system clock 16MHz, chip model (atmega328), and compile/assembly source
 *  	avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328 -c -o led/led.o led.c
 * 
 */
#include <stdio.h>

/*
 * GPIO 8-bit I/O port mapped to pin 13 (microcontroller PB5 port - attached to led) on Arduino. 
 */
char* pinB = (char*) 0x023;
char* ddrB = (char*) 0x024;
char* portB = (char*) 0x025;

char* setup_led(void) {
	
	/*
	 * PB5 (led on pin 13) configuration
	 */
	*ddrB |= (1 << 5); // Makes pin 13 (PB5) of port B as output
	
	return portB;
	
}

char get_led_state(void) {
	
    return *pinB & (1 << 5); 
    
}
